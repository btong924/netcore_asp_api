using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyAPI.Database;


namespace MyAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigDatabase(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddControllers();

            services.AddDbContext<DatabaseContext>(options =>
              options.UseSqlite(Configuration.GetConnectionString("DefaultConnection_sqlite")));

            // Create Database when Database not found
            var serviceProvider = services.BuildServiceProvider();
            DatabaseInit.INIT(serviceProvider);
        }

        public static void ConfigCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());

                options.AddPolicy("AllowSpecific", builder =>
                                {
                                    builder.WithOrigins("http://localhost:4848", "http://www.codemobiles.com")
                                            .AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                                });

                options.AddPolicy("AllowSpecificMethods", builder =>
                                {
                                    builder.WithOrigins("http://localhost:4848", "https://www.w3schools.com")
                                            .WithMethods("POST", "PUT")
                                            .AllowAnyHeader().AllowCredentials();
                                });
            });

        }
    }
}