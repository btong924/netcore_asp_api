using Microsoft.AspNetCore.Builder;
using MyAPI.Middlewaves;

namespace MyAPI.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder CustomLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LoggingMiddleware>();
        }

        public static IApplicationBuilder CustomAuth(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthMiddleware>();
        }
    }
}