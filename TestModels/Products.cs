﻿using System;
using System.Collections.Generic;

namespace MyAPI.TestModels
{
    public partial class Products
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int CategoryId { get; set; }
        public int SupplierId { get; set; }

        public virtual Categories Category { get; set; }
        public virtual Suppliers Supplier { get; set; }
    }
}
