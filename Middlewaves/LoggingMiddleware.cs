using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MyAPI.Middlewaves
{
    public class LoggingMiddleware
    {
        private RequestDelegate Next { get; }
        private ILogger<LoggingMiddleware> Logger { get; }
        public LoggingMiddleware(RequestDelegate next, ILogger<LoggingMiddleware> logger)
        {
            this.Logger = logger;
            this.Next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var path = context.Request.Path;

            Logger.LogDebug($"Request Path: {path}");

            if (!path.StartsWithSegments("/api"))
            {
                Logger.LogDebug($"Request Path: Invalid Path");
            }
            await Next(context);
        }
    }
}