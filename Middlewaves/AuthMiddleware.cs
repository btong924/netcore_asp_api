using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MyAPI.Middlewaves
{
    public class AuthMiddleware
    {
        private RequestDelegate Next { get; }
        private ILogger<AuthMiddleware> Logger { get; }

        public AuthMiddleware(RequestDelegate next, ILogger<AuthMiddleware> logger)
        {
            this.Logger = logger;
            this.Next = next;
        }

         public async Task InvokeAsync(HttpContext context)
        {
            var validKey = false;
            var keyExists = context.Request.Headers.ContainsKey("APIKey");

            if (keyExists)
            {
                if (context.Request.Headers["APIKey"].Equals("CM1234"))
                {
                    validKey = true;
                }
            }

            if (validKey)
            {
                await Next(context);
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                await context.Response.WriteAsync("Invalid API Key");
            }
        }
    }
}