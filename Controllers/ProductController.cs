using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyAPI.Database;
using MyAPI.Extensions;
using MyAPI.Models;

namespace MyAPI.Controllers
{
    [EnableCors("AllowSpecificMethods")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        public readonly EventId case_authen = 999;
        ILogger<ProductController> _logger { get; }
        private DatabaseContext DatabaseContext { get; }

        public ProductController(ILogger<ProductController> logger, DatabaseContext databaseContext)
        {
            this.DatabaseContext = databaseContext;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                IEnumerable<Product> result = DatabaseContext.Products.ToList();

                _logger.LogDebug($"Logger result : {result.Count()}");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GET Products : {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("{id}", Name = "GetProductByID")]
        public IActionResult GetProduct(int id)
        {
            _logger.LogInformation($"GetProduct : {id}", id);

            _logger.LogInformation(case_authen, $"GetProduct : {id}", id);

            try
            {
                Product result = DatabaseContext.Products.SingleOrDefault(c => c.ProductID == id);
                if (result != null)
                    return Ok(result);
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GET Product : {ex.Message}");
                return BadRequest();
            }
        }


        [HttpGet("join")]
        public IActionResult GetProductJoin()
        {
            try
            {
                IEnumerable<Product> result = DatabaseContext.Products
                .Include(c => c.ProductCategory)
                .Include(c => c.ProductSupplier)
                .ToList();
                if (result != null)
                    return Ok(result);
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GET Product : {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost]
        public IActionResult CreateProduct([FromBody] Product model)
        {
            try
            {
                DatabaseContext.Products.Add(model);
                DatabaseContext.SaveChanges();

                return CreatedAtRoute("GetProductByID", new { id = model.ProductID }, model);
            }
            catch (Exception ex)
            {
                _logger.LogError($"POST careate {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProduct([FromBody] Product model, int id)
        {
            try
            {
                Product result = DatabaseContext.Products.Find(id);
                if (result == null) return NotFound();
                result.Name = model.Name;
                result.Price = model.Price;
                result.CategoryID = model.CategoryID;
                result.SupplierID = model.SupplierID;

                DatabaseContext.Products.Update(result);
                DatabaseContext.SaveChanges();

                return Ok("Update successfully");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Update product : {ex.Message}");
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            try
            {
                Product result = DatabaseContext.Products.Find(id);
                if (result == null) return NotFound();
                DatabaseContext.Products.Remove(result);
                DatabaseContext.SaveChanges();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"DELETE product : {ex.Message}");
                return BadRequest();
            }
        }
    
        [HttpGet("bath/{money}")]
        public string PrintBath(string money){
            return money.ToBath();
        }
    }
}